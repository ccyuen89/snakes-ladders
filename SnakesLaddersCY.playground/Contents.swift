//: Playground - noun: a place where people can play

import UIKit

import Foundation

// Initial variables (just to get you started)
var numberOfPlayers = 4
var board = [String]()
var players = [Int]()
var hasWinner: Bool = false
var currentValue:Int = 0
var BackwardValue:Int = 0
var dice:Int = 0
var gameOver: Bool = false
var playerTurn: Int = 0
var boardSize: Int = 100
var currentPlayer:String = " "
var currentBroadValue: String = " "
var tempValue:Int = 0

/*-------------------------Function Start------------------------------------*/
//Board setup
func boardSetup() {
    for(var i=1; i<=boardSize+1;i++){
        board.append(" ")
    }
}

//player setup at position 0
func playersSetup() {
    for(var i=1; i<=numberOfPlayers+1;i++){
        players.append(0)
    }
}


//game play function
func gameProgress (gameContinue: Bool, _ player:Int, _ previousValue:Int)  -> Bool  {
    
    var newValue:Int
    var tempValue:Int
    dice = rollDice()
    
    //remove player from existing position
    currentPlayer = "Player \(player)"
    currentBroadValue  = board[previousValue]
    
    
    if (currentBroadValue == currentPlayer) {
        board[previousValue] = " "
    }
    
    //move player to new position
    currentValue = previousValue + dice
    tempValue = currentValue
    
    newValue = movement(currentValue, dice, player, tempValue)
    
    board[newValue] = "Player \(player)"
    players[player-1] = newValue
    
    //print steps and new position
    printStep(dice, newValue, player)
    
    //check for winners
    hasWinner = checkWinner(newValue, hasWinner)
    
    return (hasWinner)
}

func rollDice() -> Int {
    let random:Int = Int(arc4random_uniform(6))
    return random+1
}

//move position if hit more than 100
func checkLocation(movement:Int, _ newPosition:Int) -> Int {
    
    var tempPosition:Int = 0
    if newPosition > boardSize {
        BackwardValue = newPosition - boardSize
        tempPosition = boardSize - BackwardValue
    }
    else {
        tempPosition = newPosition
    }
    
    tempPosition = snake(tempPosition)
    tempPosition = ladder(tempPosition)
    return tempPosition
}

//move position if hit ladder
func ladder(currentPosition:Int) ->Int {
    var newPosition:Int = 0
    newPosition = currentPosition
    if currentPosition == 4 {
        newPosition =  14
    }
    else if currentPosition == 9 {
        newPosition = 31
    }
    else if currentPosition == 20  {
        newPosition = 38
    }
    else if currentPosition == 21 {
        newPosition = 42
    }
    else if currentPosition == 28 {
        newPosition = 84
    }
    else if currentPosition == 36 {
        newPosition = 44
    }
    else if currentPosition == 51 {
        newPosition = 67
    }
    else if currentPosition == 71 {
        newPosition = 91
    }
    else if currentPosition == 80 {
        newPosition = 99
    }
    return newPosition
    
}

//move position if hit snake
func snake (currentPosition:Int) ->Int {
    var newPosition:Int = 0
    newPosition = currentPosition
    if currentPosition == 16 {
        newPosition =  6
    }
    else if currentPosition == 49 {
        newPosition = 11
    }
    else if currentPosition == 56   {
        newPosition = 53
    }
    else if currentPosition == 64 {
        newPosition = 60
    }
    else if currentPosition == 87 {
        newPosition = 24
    }
    else if currentPosition == 93 {
        newPosition = 73
    }
    else if currentPosition == 95 {
        newPosition = 75
    }
    else if currentPosition == 98 {
        newPosition = 78
    }
    return newPosition
}

//check if hit any snake or ladder or 100
func movement (currentPosition:Int, _ dice:Int, _ player:Int, _ tempValue:Int) -> Int {
    
    var newPosition:Int
    if currentPosition > 100
    {
        newPosition = checkLocation(dice, currentPosition)
        print("Player \(player) hit more than 100 and bounced back.")
    }
    else {
        newPosition = checkLocation(dice, currentPosition)
        if newPosition < currentPosition
        {
            print("Player \(player) hit a snake and moved down.")
        }
        else if newPosition > tempValue
        {
            print("Player \(player) hit a ladder and moved up.")
            
        }
        
    }
    return newPosition
}


//game completion check
func checkWinner (currentPosition: Int, _ winner: Bool) -> Bool{
    if currentPosition == boardSize   {
        hasWinner = true
        return hasWinner
    }
    return hasWinner
}

//print steps & board
func printStep(steps:Int, _ location:Int, _ player:Int) {
    print("Player \(player) rolled a value of \(steps) and the current position is \(location)")
    
}

func printCurrentBroard ()
{
    print("---------------------------------------------------------")
    for (index, value) in board.enumerate()
    {
        print("Box \(index): \(value)")
    }
    print("---------------------------------------------------------")
    print("\n")
}

func printWinner (winner:Int) {
    print("Player \(winner) has won the game!")
}

/*-------------------------Function End--------------------------------------*/

//main
boardSetup()
playersSetup()

while (hasWinner == false) {
    
    if (playerTurn == numberOfPlayers){
        playerTurn = 1
    }
    else {
        playerTurn++
    }
    
    for (index, value) in players.enumerate()
    {
        if index == playerTurn-1 {
            currentValue = value
            tempValue = index
            break
        }
    }
    
    //printCurrentBroard ()
    hasWinner = gameProgress(hasWinner, playerTurn, currentValue)
    currentValue = 0
    
    if hasWinner
    {
        printWinner(playerTurn)
        
        for (index, value) in players.enumerate()
        {
            if index == numberOfPlayers
            {
                break
            }
            print("Last Position of player \(index+1): \(value)")
           
        }
        print("---------------------------------------------------------")
        print("\n")
    }
    
}
